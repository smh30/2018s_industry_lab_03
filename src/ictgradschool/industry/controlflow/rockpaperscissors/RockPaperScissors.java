package ictgradschool.industry.controlflow.rockpaperscissors;

import ictgradschool.Keyboard;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;


    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        System.out.println("Hi! What is your name?");
        String name = Keyboard.readInput();
        System.out.println("\n1. Rock \n2. Scissors \n3. Paper \n4. Quit \nEnter choice: ");
        int choice = Integer.parseInt(Keyboard.readInput());

        while (choice != 4) {
            System.out.println("\n");
            displayPlayerChoice(name, choice);
            int compChoice = (int) (Math.random() * 3) + 1;
            displayPlayerChoice("Computer", compChoice);
            if (userWins(choice, compChoice)) {
                System.out.println(name + " wins because " + getResultString(choice, compChoice));
            } else if (choice == compChoice) {
                System.out.println("No one wins.");
            } else {
                System.out.println("The computer wins because " + getResultString(compChoice, choice));
            }
            System.out.println("\n\n1. Rock \n2. Scissors \n3. Paper \n4. Quit \nEnter choice: ");
            choice = Integer.parseInt(Keyboard.readInput());
        }
        System.out.println("Goodbye " + name + ". Thanks for playing :)");
    }


    public void displayPlayerChoice(String name, int choice) {
        String playerChoice = "";
        if (choice == 1){
             playerChoice = "rock";
        }
        else if (choice == 2){
             playerChoice = "scissors";
        }
        else if (choice == 3){
             playerChoice = "paper";
        }
System.out.println(name + " chose " + playerChoice + ".");
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
    }

    public boolean userWins(int playerChoice, int computerChoice) {
if (playerChoice == 1 && computerChoice == 2 || playerChoice == 2 && computerChoice == 3 ||
playerChoice == 3 && computerChoice == 1){
    return true;
}
        // TODO Determine who wins and return true if the player won, false otherwise.
        return false;
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";
        if (playerChoice == 1){
            return ROCK_WINS;
        }
        else if (playerChoice == 2){
            return SCISSORS_WINS;
        }
        else if (playerChoice == 3){
            return PAPER_WINS;
        }


        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        return null;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
